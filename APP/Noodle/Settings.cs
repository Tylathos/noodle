﻿using System;
using Android.Preferences;
using Android.Content;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Linq;

namespace Noodle
{
	public static class Settings
	{
		public static string UserId { get; set; }

		public static string UserEmail { get; set; }

		public static string UserPassword { get; set; }

		public static string UserToken { get; set; }

		public static bool InstantSynch { get; set; }

		public static FullJson FullJson { get; set; }

		static Settings ()
		{
			retrieveset ();
		}

		public static void setuserdata(string useremail, string password, string usertoken, bool remember)
		{
			UserEmail = useremail;
			UserPassword = password;
			UserToken = usertoken;

			if (remember) {
				//Store
				var prefs = Android.App.Application.Context.GetSharedPreferences("Noodle", FileCreationMode.Private);
				var editor = prefs.Edit ();
				editor.PutString ("user_email", useremail);
				editor.PutString ("user_password", password);
				editor.Apply();
			}
		}

		public static void saveset()
		{
			//Store
			var prefs = Android.App.Application.Context.GetSharedPreferences("Noodle", FileCreationMode.Private);
			var editor = prefs.Edit ();
			editor.PutBoolean ("InstantSynch", InstantSynch);
			//editor.Commit();    // applies changes synchronously on older APIs
			editor.Apply();     // applies changes asynchronously on newer APIs
		}

		public static void retrieveset()
		{
			//retreive
			var prefs = Android.App.Application.Context.GetSharedPreferences("Noodle", FileCreationMode.Private);
			UserEmail = prefs.GetString("user_email", null);
			UserPassword = prefs.GetString("user_password", null);
			InstantSynch = prefs.GetBoolean ("InstantSynch", false);
		}

		public static void getUserData(bool forceRequest)
		{
			var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			path = Path.Combine (path, Noodle.Settings.UserEmail + ".json");

			if (forceRequest || !File.Exists (path)) {
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create ("http://dhbw.cloudapp.net:8080/api/Users/" + UserId);
				WebHeaderCollection headers = new WebHeaderCollection ();
				headers.Add ("Token", UserToken);
				request.Headers = headers;

				using (var reader = new System.IO.StreamReader (request.GetResponse ().GetResponseStream (), System.Text.ASCIIEncoding.UTF8)) {
					FileStream fstream = File.Create (path);
					fstream.Close ();
					File.WriteAllText (path, reader.ReadToEnd ());
				}
			}

			getJsonAsObject ();
		}

		public static void getJsonAsObject()
		{
			var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			path = Path.Combine(path, Noodle.Settings.UserEmail + ".json");
			string json = File.ReadAllText (path);
			FullJson = JsonConvert.DeserializeObject<FullJson>(json, new IsoDateTimeConverter{  DateTimeFormat = "dd.MM.YYYY"});
		}
	}
}

