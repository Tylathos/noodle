﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noodle
{
	public class Achievement
	{
		private int Id { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }

		public bool IsAchieved { get; set; }

		public int Progress { get; set; }

		public string Message { get; set; }

		public Achievement()
		{

		}

		public int get_id()
		{
			return Id;
		}
	}
}


