﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Noodle
{
    public class StudyUnit
    {
		public int ID { get; set; }
         
        public string Subject { get; set; }

		public string Name { get; set; }

        public string Description { get; set; }

		public bool IsDone { get; set; }

		public DateTime DueTo { get; set; }

		public StudyUnit()
		{

		}
    }

	public class StudyUnitGroup : List<StudyUnit>
	{
		public string Title { get; set; }
		public string ShortName { get; set; } //will be used for jump lists
		public string Subtitle { get; set; }
		public StudyUnitGroup(string title, string shortName)
		{
			Title = title;
			ShortName = shortName;
		}

		public static IList<StudyUnitGroup> All { private set; get; }
	}

	public class FullJson
	{
		private int ID { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public int Role { get; set; }
		public List<StudyUnit> StudyUnits { get; set; }
		public List<Achievement> Achievements { get; set; }

		public void save()
		{
			var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			path = Path.Combine(path, Noodle.Settings.UserEmail + ".json");
			string json = JsonConvert.SerializeObject(this, new IsoDateTimeConverter{ DateTimeFormat="s" });
			File.WriteAllText (path, json);
		}
	}
}
