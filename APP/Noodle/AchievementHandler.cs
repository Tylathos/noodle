﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using Xamarin.Forms;

namespace Noodle
{
	public class AchievementHandler
	{

		public static int handleAchievements ()
		{
			int returnval = -1;
			int counter = Settings.FullJson.Achievements.ElementAt (0).Progress;
			counter += 1;
			switch (counter) {
			case 1:
				Settings.FullJson.Achievements.ElementAt (0).IsAchieved = true;
				returnval = 0;
				pushAchievement (0);
				break;
			case 5:
				Settings.FullJson.Achievements.ElementAt (1).IsAchieved = true;
				returnval = 1;
				pushAchievement (1);
				break;
			case 10:
				Settings.FullJson.Achievements.ElementAt (2).IsAchieved = true;
				returnval = 2;
				pushAchievement (2);
				break;
			case 20:
				Settings.FullJson.Achievements.ElementAt (3).IsAchieved = true;
				returnval = 3;
				pushAchievement (3);
				break;
			case 50:
				Settings.FullJson.Achievements.ElementAt (4).IsAchieved = true;
				returnval = 4;
				pushAchievement (4);
				break;
			}
			Settings.FullJson.Achievements.ElementAt (0).Progress = counter;

			return returnval;
		}

		public static void pushAchievement(int ID)
		{
			if (Noodle.Settings.InstantSynch) {
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create ("http://dhbw.cloudapp.net:8080/api/Achievements/" + Noodle.Settings.UserId);
				WebHeaderCollection headers = new WebHeaderCollection ();
				headers.Add ("Token", Noodle.Settings.UserToken);
				request.Headers = headers;
				request.Method = "PUT";
				request.ContentType = "application/json";
				string json = "{ \"Id\": " + (ID + 1) + ", \"IsAchieved\": true }";

				using (var streamWriter = new StreamWriter (request.GetRequestStream ())) {
					streamWriter.Write (json);
				}

				try {
					request.GetResponse ();
				} catch {
					var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
					path = Path.Combine (path, Noodle.Settings.UserEmail + ".put");

					if (!File.Exists (path)) {
						FileStream fstream = File.Create (path);
						fstream.Close ();
						File.WriteAllText (path, request.RequestUri + ";" + json + "\n");
					} else {
						File.AppendAllText (path, request.RequestUri + ";" + json + "\n");
					}
				}
			} else {
				var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
				path = Path.Combine (path, Noodle.Settings.UserEmail + ".put");

				if (!File.Exists (path)) {
					FileStream fstream = File.Create (path);
					fstream.Close ();
					File.WriteAllText (path, "http://dhbw.cloudapp.net:8080/api/Achievements/" + Noodle.Settings.UserId + ";{ \"Id\": " + (ID + 1) + ", \"IsAchieved\": true }\n");
				} else {
					File.AppendAllText (path, "http://dhbw.cloudapp.net:8080/api/Achievements/" + Noodle.Settings.UserId + ";{ \"Id\": " + (ID + 1) + ", \"IsAchieved\": true }" + "\n");
				}
			}
		}
	}
}

