﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Noodle.Droid
{
	[Activity (Label = "Noodle.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			NotificationAlertReceiver alert = new NotificationAlertReceiver ();

			global::Xamarin.Forms.Forms.Init (this, bundle);

			LoadApplication (new App());
		}

		public override void OnBackPressed()
		{
			// This prevents a user from being able to hit the back button and leave the login page.
			if (Noodle.Settings.UserEmail == null)
			{
				base.OnBackPressed();
			}
		}
	}
}

