﻿using System;
using System.Collections.Generic;
using Noodle;

using Xamarin.Forms;

namespace Noodle.Droid
{
	public partial class Achievement : ContentPage
	{
		public Achievement ()
		{
			InitializeComponent ();

			listView.ItemsSource = Noodle.Settings.FullJson.Achievements;
			listView.ItemSelected += OnItemSelected;
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			//Content = new StackLayout() { Children = { new Label { Text = (listView.ItemsSource as ImageCell).ImageSource.ToString() } } };
			var item = e.SelectedItem as Noodle.Achievement;
			if (item != null) {
				Navigation.PushAsync (new AchievementView (item));
				listView.SelectedItem = null;
			}
		}
	}
}

