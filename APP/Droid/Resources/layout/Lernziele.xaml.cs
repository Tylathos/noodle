﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Converters;
using System.Linq;

namespace Noodle.Droid
{
	public partial class Lernziele : ContentPage
	{
		public Lernziele ()
		{
			InitializeComponent ();

            //Get Lernziele

			StudyUnit unit = new StudyUnit ();
			StudyUnitGroup group = new StudyUnitGroup("", "");
			List<StudyUnitGroup> items = new List<StudyUnitGroup> ();
			string old_title = "";
			Noodle.Settings.FullJson.StudyUnits = Noodle.Settings.FullJson.StudyUnits.OrderBy(o=>o.DueTo).ToList();
			Noodle.Settings.FullJson.StudyUnits = Noodle.Settings.FullJson.StudyUnits.OrderBy(o=>o.Subject).ToList();

			if (Noodle.Settings.FullJson.StudyUnits != null)
				for (int i = 0; i < Noodle.Settings.FullJson.StudyUnits.Count; i++)
				{
					unit = Noodle.Settings.FullJson.StudyUnits [i];
					
					if (old_title != unit.Subject)
					{
						if(group.Title != "")
							items.Add (group);
						group = new StudyUnitGroup (unit.Subject, unit.Subject[0].ToString());
					}
					old_title = unit.Subject;
					group.Add (unit);
				}
			items.Add (group);

			listView.ItemsSource = items;
			listView.ItemSelected += OnItemSelected;
        }

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as StudyUnit;
			if (item != null) {
				Navigation.PushAsync (new LernzielAnsicht (item));
				listView.SelectedItem = null;
			}
		}
	}
}

