﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Android.Preferences;
using System.Globalization;
using System.Net;
using System.IO;
using Android.Content;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Android.Net;
using System.Linq;

namespace Noodle.Droid
{
	public partial class LernzielAnsicht : ContentPage
	{
		StudyUnit unit;

		public LernzielAnsicht (StudyUnit unit)
		{
			InitializeComponent ();
			this.unit = unit;

			Title = unit.Name;
			su_title.Text = unit.Subject + " - " + unit.Name;
			su_description.Text = unit.Description ?? "Keine Beschreibung vorhanden!";
			su_duedate.Text = unit.DueTo.ToString("d", CultureInfo.CreateSpecificCulture("de-DE"));
			if (unit.IsDone) {
				btnfinished.Text = "Abgeschlossen";
				btnfinished.BackgroundColor = Color.Red;
				btnfinished.TextColor = Color.White;
				btnfinished.IsEnabled = false;
			} else {
				btnfinished.Text = "Abschließen";
				btnfinished.BackgroundColor = Color.Green;
				btnfinished.Clicked += finish_unit;
			}
		}

		void finish_unit(object sender, EventArgs e)
		{
			unit.IsDone = true;

			foreach (StudyUnit curunit in Noodle.Settings.FullJson.StudyUnits)
			{
				if (curunit.ID == unit.ID)
					curunit.IsDone = true;
			}

			Noodle.Settings.FullJson.save ();

			if (Noodle.Settings.InstantSynch) {
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create ("http://dhbw.cloudapp.net:8080/api/StudyUnits/" + Noodle.Settings.UserId);
				WebHeaderCollection headers = new WebHeaderCollection ();
				headers.Add ("Token", Noodle.Settings.UserToken);
				request.Headers = headers;
				request.Method = "PUT";
				request.ContentType = "application/json";
				string json = "{ \"Id\": " + this.unit.ID + ", \"IsDone\": true }";

				try {
					using (var streamWriter = new StreamWriter (request.GetRequestStream ())) {
						streamWriter.Write (json);
					}
					request.GetResponse ();
				} catch (Exception ex) {
					if (ex.Message == "Error: NameResolutionFailure" || ex.Message == "Error: ConnectFailure (Network is unreachable)")
						DisplayAlert ("Warnung", "Verbindung zum Server konnte nicht hergestellt werden.", "Ok");
					else {
						DisplayAlert ("Fehler", ex.Message + "\nSie können diese Fehlermeldung für ein Ticket an den Support verwenden.", "Ok");
					}
					DisplayAlert ("Lernziel abschließen", "Die Aktualisierung des Lernziels am Server ist fehlgeschlagen.\nBei der nächsten Synchronisation wird versucht die Aktualisierung erneut auszuführen!", "Ok");

					var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
					path = Path.Combine (path, Noodle.Settings.UserEmail + ".put");

					if (!File.Exists (path)) {
						FileStream fstream = File.Create (path);
						fstream.Close ();
						File.WriteAllText (path, request.RequestUri + ";" + json + "\n");
					} else {
						File.AppendAllText (path, request.RequestUri + ";" + json + "\n");
					}
				}
			} else {
				var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
				path = Path.Combine (path, Noodle.Settings.UserEmail + ".put");

				if (!File.Exists (path)) {
					FileStream fstream = File.Create (path);
					fstream.Close ();
					File.WriteAllText (path, "http://dhbw.cloudapp.net:8080/api/StudyUnits/" + Noodle.Settings.UserId + ";{ \"Id\": " + this.unit.ID + ", \"IsDone\": true }\n");
				} else {
					File.AppendAllText (path, "http://dhbw.cloudapp.net:8080/api/StudyUnits/" + Noodle.Settings.UserId + ";{ \"Id\": " + this.unit.ID + ", \"IsDone\": true }" + "\n");
				}
			}

			btnfinished.Text = "Abgeschlossen";
			btnfinished.BackgroundColor = Color.Red;
			btnfinished.TextColor = Color.White;
			btnfinished.IsEnabled = false;

			int achievedId = AchievementHandler.handleAchievements ();
			if(achievedId != -1) {
				DisplayAlert ("Achievement erreicht!", Noodle.Settings.FullJson.Achievements.ElementAt (achievedId).Name + ":\n" + Noodle.Settings.FullJson.Achievements.ElementAt (achievedId).Description, "Ok");
			}
		}
	}
}

