﻿using System;
using System.Collections.Generic;
using Android.App;

using Xamarin.Forms;
using System.IO;
using System.Net;

namespace Noodle.Droid
{
	public partial class Settings : ContentPage
	{
		public Settings ()
		{
			InitializeComponent ();
			btnlogout.Clicked += Logout;
			btnsynch.Clicked += Synch;
			sautosynch.IsToggled = Noodle.Settings.InstantSynch;
			sautosynch.Toggled += Sautosynch_Toggled; 
		}

		void Sautosynch_Toggled (object sender, ToggledEventArgs e)
		{
			Noodle.Settings.InstantSynch = ((Switch)sender).IsToggled;
			Noodle.Settings.saveset ();
		}

		protected void Logout(object sender, EventArgs e)
		{
			Noodle.Settings.setuserdata (null, null, null, true);
			Navigation.PushModalAsync (new NavigationPage (new Login ()));
		}

		protected void Synch(object sender, EventArgs e)
		{
			try{
				var path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
				path = Path.Combine (path, Noodle.Settings.UserEmail + ".put");

				string line;
				if(File.Exists(path)) {					
					StreamReader fstream = new StreamReader (path);
					while(!string.IsNullOrEmpty(line = fstream.ReadLine())) {
						string[] data = line.Split(new char[] { ';' });
						HttpWebRequest request = (HttpWebRequest)WebRequest.Create (data[0]);
						WebHeaderCollection headers = new WebHeaderCollection ();
						headers.Add ("Token", Noodle.Settings.UserToken);
						request.Headers = headers;
						request.Method = "PUT";
						request.ContentType = "application/json";

						using (var streamWriter = new StreamWriter (request.GetRequestStream ())) {
							streamWriter.Write (data[1]);
						}

						try {
							request.GetResponse ();
						}
						catch (Exception ex){
							if (ex.Message == "Error: NameResolutionFailure" || ex.Message == "Error: ConnectFailure (Network is unreachable)")
								DisplayAlert ("Warnung", "Verbindung zum Server konnte nicht hergestellt werden. Sie können die App weiter offline benutzen", "Ok");
							else {
								DisplayAlert ("Fehler", ex.Message + "\nSie können diese Fehlermeldung für ein Ticket an den Support verwenden.", "Ok");
							}
						}
					}
				}

				Noodle.Settings.getUserData (true);
				DisplayAlert ("Synchronisieren", "Deine Daten wurden erfolgreich vom Server geladen!", "Ok");
			}
			catch(Exception ex) {
				if (ex.InnerException.Message == "Error: NameResolutionFailure" || ex.Message == "Error: ConnectFailure (Network is unreachable)")
					DisplayAlert ("Warnung", "Verbindung zum Server konnte nicht hergestellt werden. Sie können die App weiter offline benutzen", "Ok");
				else {
					DisplayAlert ("Fehler", ex.Message + "\nSie können diese Fehlermeldung für ein Ticket an den Support verwenden.", "Ok");
				}
			}
		}
	}
}

