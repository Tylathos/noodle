﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Noodle.Droid
{
	public partial class AchievementView : ContentPage
	{
		public AchievementView (Noodle.Achievement achievement)
		{
			InitializeComponent ();

			Title = achievement.Name;
			su_title.Text = Title;
			su_description.Text = achievement.Description;
		}
	}
}

