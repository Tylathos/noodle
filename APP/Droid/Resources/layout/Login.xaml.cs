﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Noodle.Droid
{
	public partial class Login : ContentPage
	{
		public Login ()
		{
			InitializeComponent ();
			btnlogin.Clicked += checkLogin;
		}

		protected void checkLogin(object sender, EventArgs e)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(epassword.Text);
			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("X2"));
			}

			WebRequest request = WebRequest.Create("http://dhbw.cloudapp.net:8080/api/Usertoken?username=" + email.Text + "&password=" + sb.ToString().ToLower());
			string[] responseText;

			try {				
				using (var reader = new System.IO.StreamReader(request.GetResponse().GetResponseStream(), System.Text.ASCIIEncoding.ASCII))
				{
					responseText = reader.ReadToEnd().Replace("\"", "").Split(new char[] { ';' });
				}
			}
			catch(Exception ex) {
				if (ex.Message == "Error: NameResolutionFailure")
					DisplayAlert ("Warnung", "Verbindung zum Server konnte nicht hergestellt werden. Überprüfen Sie ihre Internetverbindung!", "Ok");
				else {
					lerror.IsVisible = true;
				}
				return;
			}

			Noodle.Settings.setuserdata (email.Text, sb.ToString().ToLower(), responseText[1], eremember.IsToggled);
			Noodle.Settings.UserId = responseText[0];

			Noodle.Settings.getUserData(false);

			Navigation.PopModalAsync ();

			var detail = new Lernziele ();
			((MasterDetailPage)Application.Current.MainPage).Detail = new NavigationPage(detail);
			((MasterDetailPage)Application.Current.MainPage).Title = detail.Title;
		}
	}
}

