﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Noodle.Droid
{
	public partial class MasterPage : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public MasterPage ()
		{
			InitializeComponent ();

			var masterPageItems = new List<MasterPageItem> ();
			masterPageItems.Add (new MasterPageItem {
				Title = "Lernziele",
				IconSource = "../drawable/list.png",
				TargetType = typeof(Lernziele)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Achievements",
				IconSource = "../drawable/Achievement.png",
				TargetType = typeof(Achievement)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Einstellungen",
				IconSource = "../drawable/settings.png",
				TargetType = typeof(Settings)
			});

			listView.ItemsSource = masterPageItems;
		}
	}
}

