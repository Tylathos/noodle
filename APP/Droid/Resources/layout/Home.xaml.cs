﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Net;

namespace Noodle.Droid
{
	public partial class Home : MasterDetailPage
	{
		public Home ()
		{
			InitializeComponent ();
			masterPage.ListView.ItemSelected += OnItemSelected;
		}

		protected override void OnAppearing()
		{
			//Anmeldedaten lokal gespeichert?
			if (string.IsNullOrEmpty(Noodle.Settings.UserEmail)) {
				Navigation.PushModalAsync (new NavigationPage (new Login ()));
			} else {
				Noodle.Settings.getJsonAsObject ();
				try {
					WebRequest request = WebRequest.Create("http://dhbw.cloudapp.net:8080/api/Usertoken?username=" + Noodle.Settings.UserEmail + "&password=" + Noodle.Settings.UserPassword);

					using (var reader = new System.IO.StreamReader(request.GetResponse().GetResponseStream(), System.Text.ASCIIEncoding.ASCII))
					{
						string[] responsedata = reader.ReadToEnd().Replace("\"", "").Split(new char[] { ';' });
						Noodle.Settings.UserId = responsedata[0];
						Noodle.Settings.UserToken = responsedata[1];
					}
				}
				catch(Exception e) {
					if (e.Message == "Error: NameResolutionFailure")
						DisplayAlert ("Warnung", "Verbindung zum Server konnte nicht hergestellt werden. Sie können die App weiter offline benutzen", "Ok");
					else {
						Noodle.Settings.setuserdata (null, null, null, false);
						Navigation.PushModalAsync (new NavigationPage (new Login ()));
					}
				}

				var detail = new Lernziele ();
				Detail = new NavigationPage(detail);
				Title = detail.Title;

				int duetoday = 0;
				foreach(StudyUnit unit in Noodle.Settings.FullJson.StudyUnits)
					if (unit.DueTo.ToShortDateString () == DateTime.Now.ToShortDateString ())
						duetoday++;

				DisplayAlert ("Heute fällig", "Heute ist/sind " + duetoday + " Lernziel(e) fällig.", "Ok");
			}

			base.OnAppearing();
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
			if (item != null) {
				Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
				Title = item.Title;
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}
	}
}

