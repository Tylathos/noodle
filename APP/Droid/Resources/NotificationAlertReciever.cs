﻿using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using System;

namespace Noodle.Droid
{
	[BroadcastReceiver]
	public class NotificationAlertReceiver : BroadcastReceiver
	{
		public NotificationAlertReceiver()
		{

		}

		public override void OnReceive(Context context, Intent intent)
		{
			PowerManager pm = (PowerManager)context.GetSystemService(Context.PowerService);
			PowerManager.WakeLock w1 = pm.NewWakeLock (WakeLockFlags.Partial, "NotificationReceiver");
			w1.Acquire ();
			var nMgr = (NotificationManager)context.GetSystemService (Context.NotificationService);
			var notification = new Notification (Resource.Drawable.icon, intent.Categories.ElementAt(1));
			var pendingIntent = PendingIntent.GetActivity (context, 0, new Intent (context, typeof(MainActivity)), 0);
			notification.SetLatestEventInfo (context, intent.Categories.ElementAt(1), intent.Categories.ElementAt(0), pendingIntent);
			nMgr.Notify (0, notification);
			w1.Release ();
		}

		public void CancelAlarm(Context context){ 
			Intent intent = new Intent(context, this.Class);
			PendingIntent sender = PendingIntent.GetBroadcast (context, 0, intent, 0);
			AlarmManager alarmManager = (AlarmManager) context.GetSystemService (Context.AlarmService);
			alarmManager.Cancel (sender);
		}

		public void SetAlarm(Context context, long alertTime, string title, string text){
			long now = SystemClock.ElapsedRealtime();
			AlarmManager am = (AlarmManager) context.GetSystemService (Context.AlarmService);
			Intent intent = new Intent(context, this.Class);
			intent.AddCategory (title);
			intent.AddCategory (text);
			PendingIntent pi = PendingIntent.GetBroadcast (context, 0, intent, 0);
			am.Set (AlarmType.ElapsedRealtimeWakeup, now + (alertTime / 10000), pi);
		}
	}
}