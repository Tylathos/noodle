<!DOCTYPE html>
<?php
session_start();
$header = array();
$header[] = 'Token: ' . $_SESSION['token'];
$id = $_SESSION['id'];
$service_URL = 'http://dhbw.cloudapp.net:8080/api/Users/' . $id;
$curl = curl_init($service_URL);
curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
$curl_response = curl_exec($curl);
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);
	die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
$info = curl_getinfo($curl);
curl_close($curl);
$decoded = json_decode($curl_response);
if(isset($_GET['id'])){
	$_SESSION['bla'] = $_GET['id'];
	$studyunits = $decoded->{'StudyUnits'};
	$anzahl = count($studyunits);
	$subjects = array();
	if($anzahl > 0){
		$unit = current($studyunits);
	}
	for($i = $anzahl; $i > 0; $i--){
		if($unit->{'Id'}== $_GET['id']){
		break;
		}
		$unit = next($studyunits);
	}
	$_SESSION['unit'] = $unit;
}
?>

<?php
	if(isset($_GET['update'])) {
		$subject = $_POST['fach'];
		$name = $_POST['name'];
		$description = $_POST['beschreibung'];
		$due = $_POST['datum'];
		$id = $_POST['id'];
		$done = boolval($_POST['fertig']);
		
		
		$unit = array(
			"Id" => $id,
			"Subject" => $subject,
			"Name" => $name,
			"Description" => $description,
			"IsDone" => $done,
			"DueTo" => $due,
		);
		$json = json_encode($unit);
		$header = array('Token: ' . $_SESSION['token'] , 'Content-Type: application/json');
		//$header[] = 'Token: ' . $_SESSION['token'] , 'Content-Type: application/json';
		$id = $_SESSION['id'];
		//Bene Paramter fragen
		$service_URL = 'http://dhbw.cloudapp.net:8080/api/Studyunits/' . $id;//. "?StudyUnit=".$json
		echo "<br>";
		echo "<br>";
		echo "<br>";
		echo "<br>";
		echo "<br>";
		$bla = "StudyUnit=".$json;
		echo $service_URL;
		$curl = curl_init($service_URL);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($curl, CURLOPT_POSTFIELDS,$json);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		$info = curl_getinfo($curl);
		curl_close($curl);
		if ($info['http_code'] === 200){
			header ("LOCATION: studyunits.php");
		}
		elseif($info['http_code'] === 500){
			echo "Fehler aufgetretten!";
			header ("LOCATION: changestudyunit.php?=".$id);
		}
		echo var_dump($unit);
	}
?>



<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dein Noodle Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.php">Noodle</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="profile.php"> <?php $user = $_SESSION['user']; echo $user;?></a></li>
            <li><a href="helppage.php">Hilfe</a></li>
			<li><form action="logout.php" method="post">
			<button type="submit" id="btnLogin" class="btn btn-default navbar-btn">ausloggen</button>
			</form></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="dashboard.php">�bersicht</a></li>
            <li class="active"><a href="studyunits.php" >Lernziele <span class="sr-only">(current)</span></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<h1 class="page-header">Bearbeiten des Lernziel</h1> 
			
				<form role="form" action="?update=1" method="post">
					<input type="hidden" name="id" value="<?php echo $_GET['id']?>">
					<div class="form-group">
						<label for="fach">Fach:</label>
						<input name="fach" type="fach" class="form-control" id="fach" value="<?php echo $_SESSION['unit']->{'Subject'};?>">
					</div>
					<div class="form-group">
						<label for="name">Name:</label>
						<input name="name" type="name" class="form-control" id="name" value="<?php echo $_SESSION['unit']->{'Name'};?>">
					</div>
					<div class="form-group">
						<label for="beschreibung">Beschreibung:</label>
						<textarea name="beschreibung" class="form-control" id="beschreibung" rows="5" ><?php echo $_SESSION['unit']->{'Description'};?></textarea>
					</div>
					<div class="form-group">
						<label for="datum">Fertig zum:</label>
						<input type="datetime-local" name="datum" value="<?php echo $_SESSION['unit']->{'DueTo'};?>">
					</div>
					<div class="radio">
					<?php 
					if ($_SESSION['unit']->{'IsDone'}){
						echo "<label class=\"radio-inline\"><input type=\"radio\" name=\"fertig\" value=\"1\" checked>Schon Erledigt</label>";
						echo "<label class=\"radio-inline\"><input type=\"radio\" name=\"fertig\" value=\"0\" >Zu erledigen</label>";
					}
					else{
						echo "<label class=\"radio-inline\"><input type=\"radio\" name=\"fertig\" value=\"1\" >Schon Erledigt</label>";
						echo "<label class=\"radio-inline\"><input type=\"radio\" name=\"fertig\" value=\"0\" checked>Zu erledigen</label>";
					}
					?>
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
          
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
