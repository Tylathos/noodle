<!DOCTYPE html>
<?php
	session_start();
	if(isset($_GET['create'])) {
		$user = $_POST['username'];
		$password = $_POST['password'];
		$password_r = $_POST['password_rep'];
		if ($password != $password_r){break;}
		$hash = md5($password);
		$service_URL = 'http://dhbw.cloudapp.net:8080/api/users';
		$unit = array(
			"Username" => $user,
			"Password" => $hash,
			"Role" => 1,
		);
		$json = json_encode($unit);
		echo $json;
		$curl = curl_init($service_URL);
		//curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
		//curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt_array($curl, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_POSTFIELDS => $json
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
		$info = curl_getinfo($curl);
		curl_close($curl);
		If ($info['http_code'] === 201){ 
			header ("LOCATION: index.php"); 
		}
		elseif($info['http_code'] == 500){
			Echo "Sonstiger Fehler aufgetreten, Bitte probieren sie es erneut oder Kontaktieren sie den Support ( den es nicht gibt!)";
		}

	}
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico"> 

    <title>Noodle - Registrieren</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body background="Pictures/blue-wallpaper-7.jpg">

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="inner cover">
            <h1 class="cover-heading">Registriere dich bei Noodle</h1>
            <p class="lead">Trage deine Daten ein und es kann losgehen!</p>
            <p class="lead">
				<form action="?create=1" class="form" id="formRegister" method="post"> 
                <input name="username" id="username" type="text" placeholder="Username"></br>
                <input name="password" id="password" type="password" placeholder="Password"></br>
				<input name="password_rep" id="password_rep" type="password" placeholder="Password wiederholen"></br> </br>
				<button type="submit" id="btnLogin" class="btn btn-lg btn-default">Registrieren</button>
                </form>
            </p>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>