<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dein Noodle Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Noodle</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="profile.php"> <?php session_start(); $user = $_SESSION['user']; echo $user;?></a></li>
            <li><a href="helppage.php">Hilfe</a></li>
	        <li><form action="logout.php" method="post">
			<button type="submit" id="btnLogin" class="btn btn-default navbar-btn">ausloggen</button>
			</form></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="dashboard.php">Übersicht</a></li>
            <li><a href="studyunits.php">Lernziele</a></li>
            <li class="active"><a href="showachievements.php">Achievements <span class="sr-only">(current)</span></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Achievements</h1>


          <h2 class="sub-header">Erreichte Achievements</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Titel</th>
                  <th>Beschreibung</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                </tr>
              </tbody>
            </table>
          </div>

          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>