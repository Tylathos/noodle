<!DOCTYPE html>
<?php
session_start();
$header = array();
$header[] = 'Token: ' . $_SESSION['token'];
$id = $_SESSION['id'];
$service_URL = 'http://dhbw.cloudapp.net:8080/api/Users/' . $id;
$curl = curl_init($service_URL);
curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
$curl_response = curl_exec($curl);
if ($curl_response === false) {
	$info = curl_getinfo($curl);
	curl_close($curl);
	die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
$info = curl_getinfo($curl);
curl_close($curl);
$decoded = json_decode($curl_response);
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dein Noodle Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.php">Noodle</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="profile.php"> <?php $user = $_SESSION['user']; echo $user;?></a></li>
            <li><a href="helppage.php">Hilfe</a></li>
			<li><form action="logout.php" method="post">
			<button type="submit" id="btnLogin" class="btn btn-default navbar-btn">ausloggen</button>
			</form></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="dashboard.php">&Uumlbersicht</a></li>
            <li class="active"><a href="studyunits.php" >Lernziele <span class="sr-only">(current)</span></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Deine Lernziele <form action="studycreate.php" method="post"><button type="submit" id="btnmilestone" class="btn btn-lg btn-default" >+</button> </form></h1> 
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Anzahl Unteraufgaben</th>
                  <th>davon noch offen</th>
                  <th>erledigt in %</th>
                </tr>
              </thead>
              <tbody>
			<?php
			$studyunits = $decoded->{'StudyUnits'};
			
			$anzahl = count($studyunits);
			$subjects = array();
			if($anzahl > 0){
				$unit = current($studyunits);
			}
			for($i = $anzahl; $i > 0; $i--){

				if(array_key_exists($unit->{'Subject'},$subjects)){
					$subject = $unit->{'Subject'};
					$anzahl = $subjects[$subject]['Anzahl'];
					$offen = $subjects[$subject]['Offen'];
					$anzahl = $anzahl + 1;
					if ($unit->{'IsDone'}){
						
					}
					else{$offen= $offen +1;}
					$subjects[$subject]['Anzahl'] = $anzahl;
					$subjects[$subject]['Offen'] = $offen;
				}
				else{
					$name = $unit->{'Subject'};
					$offen = $unit->{'IsDone'};
					if ($offen){
						$offen = 0;
					}
					else{
						$offen = 1;
					}
					$subjects[$name] = array ( "Anzahl" => 1,"Offen"=> $offen,);
					
					
				}
				$unit = next($studyunits);
			}
			$anzahl = count($subjects);
			$names = array_keys($subjects);
			if($anzahl > 0){
				$unit = current($subjects);
			}
			for ($i = 0; $i < $anzahl;$i++){
				$b = $i+1;
				$prozent = $unit['Offen'] / $unit['Anzahl'];
				$prozent = $prozent *100;
				$prozent = 100 - $prozent;
				echo  "<tr>";
                echo  "<td>".$b."</td>";
                echo  "<td><a href=\"explicitstudyunit.php?name=".$names[$i]."\">".$names[$i]."</a></td>";
                echo  "<td>".$unit['Anzahl']."</td>";
				echo  "<td>".$unit['Offen']."</td>";
				echo  "<td>".$prozent."</td>";
                echo  "</tr>";
				$unit = next($subjects);
			}
			?>
              
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
