﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoodleServer.Models
{
    public class StudyUnit
    {
        public int Id;
        public string Subject { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }
        public DateTime DueTo { get; set; }
    }
}