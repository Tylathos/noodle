﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;

namespace NoodleServer.Models
{
    public class UserData
    {
        public int Id;
        public string Username { get; set; }
        public string Password { get; set; }
        public AuthenticationTypes Role;

        public List<StudyUnit> StudyUnits { get; set; }

        public List<Achievement> Achievements { get; set; }

        public List<int> Children { get; set; }

        public Guid UserToken = new Guid();
  
        private Timer tokenRefreshTimer;

        public event EventHandler GuidChanged;

        protected virtual void OnGuidChanged(EventArgs e)
        {
            EventHandler handler = GuidChanged;
            if(handler != null)
            {
                handler(this, e);
            }
        }

        public UserData()
        {
            UserToken = Guid.NewGuid();
            SetUpTimer();
        }

        private void SetUpTimer()
        {
            tokenRefreshTimer = new Timer();
            tokenRefreshTimer.Interval = 1800000;
            tokenRefreshTimer.Elapsed += GenerateUserToken;
            tokenRefreshTimer.AutoReset = true;
            tokenRefreshTimer.Start();
        }

        public void GenerateUserToken(object sender, ElapsedEventArgs e)
        {
            UserToken = Guid.NewGuid();
            OnGuidChanged(new EventArgs());
        }

        public void ResetTimer()
        {
            if(tokenRefreshTimer == null)
            {
                SetUpTimer();
            } else
            {
                tokenRefreshTimer.Stop();
                tokenRefreshTimer.Start();
            }
            
        }
    }
}