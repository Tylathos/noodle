﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NoodleServer.Models
{
    public class Achievement
    {
        public int Id;
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAchieved { get; set; }
        public int Progress { get; set; }
        public string Message { get; set; }
    }
}