﻿using NoodleServer.BackgroundServices;
using NoodleServer.Helper;
using NoodleServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NoodleServer.Controllers
{
    public class StudyUnitsController : ApiController
    {
        // POST: api/StudyUnits
        public IHttpActionResult Post([FromBody]StudyUnit newStudyUnit, int id)
        {
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if(searchResult == null)
            {
                return InternalServerError();
            } else
            {
                if (Request.Headers.Contains("Token"))
                {
                    if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.PARENT, searchResult))
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }

                if (searchResult.StudyUnits == null)
                {
                    searchResult.StudyUnits = new List<StudyUnit>();
                    newStudyUnit.Id = 1;
                } 

                if(newStudyUnit.Name != null && newStudyUnit.Subject != null && newStudyUnit.Description != null)
                {
                    if(newStudyUnit.Id == 0 && searchResult.StudyUnits.Count != 0)
                    {
                        newStudyUnit.Id = searchResult.StudyUnits.LastOrDefault().Id + 1;
                    } else
                    {
                        newStudyUnit.Id = 1;
                    }

                    newStudyUnit.IsDone = false;

                    searchResult.StudyUnits.Add(newStudyUnit);
                    searchResult.ResetTimer();
                    UserService.Instance.WriteUserToDb(searchResult, false);
                } else
                {
                    return InternalServerError();
                }

                

                return Created("",newStudyUnit);
            }
        }

        // PUT: api/StudyUnits/5
        public IHttpActionResult Put(int id, [FromBody]StudyUnit newValues)
        {
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if(searchResult == null)
            {
                return InternalServerError();
            } else
            {
                if (Request.Headers.Contains("Token"))
                {
                    if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.STUDENT, searchResult))
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }

                if(UserService.Instance.UpdateStudyUnit(searchResult, newValues))
                {
                    return Ok();
                } else
                {
                    return InternalServerError();
                }

            }
        }

        // DELETE: api/StudyUnits/5
        public IHttpActionResult Delete(int studyUnitId, int userId)
        {
            if(studyUnitId == 0 || userId == 0)
            {
                return InternalServerError();
            }
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == userId);

            if(searchResult == null)
            {
                return InternalServerError();
            }

            if (Request.Headers.Contains("Token"))
            {
                if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.PARENT, searchResult))
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

            StudyUnit studyUnitToBeDeleted = searchResult.StudyUnits.FirstOrDefault(x => x.Id == studyUnitId);
                
                
            if(studyUnitToBeDeleted != null )
            {
                searchResult.StudyUnits.Remove(studyUnitToBeDeleted);
                UserService.Instance.WriteUserToDb(searchResult, false);
                return Ok();
            } else
            {
                return InternalServerError();
            }
        }
    }
}
