﻿using NoodleServer.BackgroundServices;
using NoodleServer.Helper;
using NoodleServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NoodleServer.Controllers
{
    public class AchievementsController : ApiController
    {
        // POST: api/Achievements
        public IHttpActionResult Post([FromBody]Achievement newAchievement, int id)
        {
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if(searchResult == null)
            {
                return InternalServerError();
            } else
            {
                if (Request.Headers.Contains("Token"))
                {
                    if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.STUDENT, searchResult))
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }

                if(searchResult.Achievements == null)
                {
                    searchResult.Achievements = new List<Achievement>();
                }
                if(newAchievement != null && newAchievement.Description != null && newAchievement.Name != null)
                {
                    if(searchResult.Achievements.Count != 0)
                    {
                        newAchievement.Id = searchResult.Achievements.LastOrDefault().Id + 1;
                    } else
                    {
                        newAchievement.Id = 1;
                    }

                    searchResult.Achievements.Add(newAchievement);
                    searchResult.ResetTimer();
                    UserService.Instance.WriteUserToDb(searchResult, false);

                    return Created("", newAchievement);
                } else
                {
                    return InternalServerError();
                }


            }
        }

        // PUT: api/Achievements/5
        public IHttpActionResult Put(int id, [FromBody]Achievement newAchievement)
        {
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if(searchResult == null)
            {
                return InternalServerError();
            } else
            {
                if (Request.Headers.Contains("Token"))
                {
                    if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.STUDENT, searchResult))
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }

                if (UserService.Instance.UpdateAchievement(searchResult, newAchievement))
                {
                    return Ok();
                } else
                {
                    return InternalServerError();
                }
            }
        }
    }
}
