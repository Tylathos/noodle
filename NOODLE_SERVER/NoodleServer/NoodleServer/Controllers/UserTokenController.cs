﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDbAccessLib;
using NoodleServer.BackgroundServices;
using NoodleServer.Models;

namespace NoodleServer.Controllers
{
    public class UserTokenController : ApiController
    {
        // GET: api/UserToken
        public IHttpActionResult Get(string username, string password)
        {
            UserData temp = UserService.Instance.Users.Where(x => x.Username == username && x.Password == password).FirstOrDefault();

            if(temp != null)
            {
                temp.ResetTimer();
                return Ok(String.Format("{0};{1}",temp.Id,temp.UserToken));
            } else
            {
                return Unauthorized();
            }

        }

    }
}
