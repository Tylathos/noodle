﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using NoodleServer.Models;
using NoodleServer.BackgroundServices;
using NoodleServer.Helper;

namespace NoodleServer.Controllers
{
    public class UsersController : ApiController
    {

        // GET: api/users/5
        public IHttpActionResult Get(int id)
        {
            UserData searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if (searchResult != null && Request.Headers.Contains("Token"))
            {
                if(AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.STUDENT, searchResult))
                {
                    //searchResult.ResetTimer();
                    return Ok(searchResult);
                } else
                {
                    return Unauthorized();
                }
            } else
            {
                return InternalServerError();
            }
            

            
        }

        // POST: api/users
        //TODO: Check for infiltrated data!!! (DONE)
        public IHttpActionResult Post([FromBody]UserData newUser)
        {
            if (newUser.Username != null && newUser.Password != null && newUser.Role != 0)
            {
                //Prevent user from infiltrating data:
                newUser.Achievements = new List<Achievement>();
                newUser.Children = new List<int>();
                newUser.StudyUnits = new List<StudyUnit>();
                newUser.UserToken = Guid.NewGuid();

                if (UserService.Instance.Users.Count > 0)
                {
                    newUser.Id = UserService.Instance.Users.LastOrDefault().Id + 1;

                    //Check if User with same username exists
                    if(UserService.Instance.Users.FirstOrDefault(x => x.Username == newUser.Username) != null)
                    {
                        return InternalServerError();
                    }
                }
                else
                {
                    newUser.Id = 1;
                }

                UserService.Instance.AddUser(newUser);

                UserService.Instance.WriteUserToDb(newUser, true);

                return Created("", newUser);
            }
            else
            {
                return InternalServerError();
            }
            
        }

        // PUT: api/users
        public IHttpActionResult Put([FromBody]UserData value)
        {

            if (Request.Headers.Contains("Token"))
            {
                if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.ADMIN, null))
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }


            if (UserService.Instance.UpdateUser(value))
            {
                return Ok();
            } else
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            
        }

        // DELETE: api/users/5
        public IHttpActionResult Delete(int id)
        {

            if (Request.Headers.Contains("Token"))
            {
                if (!AuthenticationHelper.CheckForAuthorization(Guid.Parse(Request.Headers.GetValues("Token").First()), AuthenticationTypes.ADMIN, null))
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

            UserData userToRemove = UserService.Instance.Users.FirstOrDefault(x => x.Id == id);

            if(UserService.Instance.Users.Remove(userToRemove))
            {
                UserService.Instance.DeleteUserFromDb(userToRemove);

                return Ok();
            } else
            {
                return InternalServerError();
            }
        }
    }
}
