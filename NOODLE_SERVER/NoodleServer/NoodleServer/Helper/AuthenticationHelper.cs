﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NoodleServer.BackgroundServices;
using NoodleServer.Models;

namespace NoodleServer.Helper
{
    public class AuthenticationHelper
    {

        public static bool CheckForAuthorization(Guid requestToken, AuthenticationTypes authenticationType, UserData accessedObject )
        {
            UserData requestingUser = UserService.Instance.Users.FirstOrDefault(x => x.UserToken == requestToken);
        
            if((accessedObject == null && authenticationType != AuthenticationTypes.ADMIN )|| requestingUser == null)
            {
                return false;
            }

            //Reset the guid Timer of the requesting user:
            requestingUser.ResetTimer();

            switch (authenticationType)
            {
                case AuthenticationTypes.ADMIN:
                    if(requestingUser.Role == AuthenticationTypes.ADMIN)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                case AuthenticationTypes.PARENT:
                    if(requestingUser.Role == AuthenticationTypes.PARENT)
                    {
                        return checkParentAuthentication(accessedObject, requestingUser);
                    } else if(requestingUser.Role == AuthenticationTypes.ADMIN)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                case AuthenticationTypes.STUDENT:
                    if (requestingUser.Role == AuthenticationTypes.STUDENT && requestingUser.UserToken.Equals(accessedObject.UserToken))
                    {
                        return true;
                    } else if(requestingUser.Role == AuthenticationTypes.PARENT)
                    {
                        return checkParentAuthentication(accessedObject, requestingUser);
                    } else if(requestingUser.Role == AuthenticationTypes.ADMIN)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                default:
                    return false;

            }
        }

        private static bool checkParentAuthentication(UserData accessedObject, UserData requestingUser)
        {
            if(accessedObject != null && requestingUser != null)
            {
                if (accessedObject.Equals(requestingUser))
                {
                    return true;
                }
                else
                {
                    if (requestingUser.Children != null && requestingUser.Children.Contains(accessedObject.Id))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            } else
            {
                return false;
            }
            
            
        }
    }
}