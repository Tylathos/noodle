﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace NoodleServer.Helper
{
    public class UpdateHelper
    {

        public static void UpdateObject<T>(T source, T destination)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();

            foreach(PropertyInfo property in properties)
            {
                if(property.CanWrite && !CheckIfNullOrDefault(property.PropertyType,property.GetValue(source))){
                    property.SetValue(destination, property.GetValue(source));
                }
            }
        }

        public static bool CheckIfNullOrDefault(Type propertyType,object propertyValue)
        {
            if(propertyType == typeof(bool))
            {
                return false;
            } else if(propertyValue != null || propertyType.IsValueType)
            {
                return object.Equals(propertyValue, GetDefaultValue(propertyType));
            } else
            {
                return true;
            }
        }

        public static object GetDefaultValue(Type propertyType)
        {
            if (propertyType.IsValueType)
            {
                return Activator.CreateInstance(propertyType);
            } else
            {
                return null;
            }
        }


    }
}