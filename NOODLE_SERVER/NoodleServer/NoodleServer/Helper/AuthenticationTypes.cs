﻿public enum AuthenticationTypes
{
    ADMIN = 1,
    PARENT = 2,
    STUDENT = 3
}