﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDbAccessLib;
using NoodleServer.Models;
using System.Timers;
using System.Collections.ObjectModel;
using System.Reflection;
using NoodleServer.Helper;

namespace NoodleServer.BackgroundServices
{
    public class UserService
    {
        //Singleton pattern
        private static UserService instance;

        public static UserService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserService();
                }
                return instance;
            }
        }

        private List<UserData> users;

        private MongoDbAccess dbAccessor = new MongoDbAccess("mongodb://localhost", "Noodle");

        public List<UserData> Users
        {
            get
            {
                if(users == null)
                {
                    this.GetAllUsers();
                }
                return users;
            }
        }

        public UserService()
        {
            GetAllUsers();
        }


        private void GetAllUsers()
        {
            this.users = dbAccessor.FindMany<UserData>("").ToList<UserData>();
            foreach(UserData currentUser in this.users)
            {
                currentUser.GuidChanged += GuidChangedEventHandler;
            }
        }

        public void AddUser(UserData newUser)
        {
            newUser.Achievements.Add(new Achievement() { Id = 1, Name = "Get started", Description = "Du hast dein erstes Mal mit der Noodle App gelernt!", IsAchieved = false, Progress = 0 });
            newUser.Achievements.Add(new Achievement() { Id = 2, Name = "Noodlegericht", Description = "Du hast das fünfte Mal mit der Noodle App gelernt!", IsAchieved = false, Progress = 0 });
            newUser.Achievements.Add(new Achievement() { Id = 3, Name = "Ich bin eine gute Noodle", Description = "Du hast das zehnte Mal mit der Noodle App gelernt!", IsAchieved = false, Progress = 0 });
            newUser.Achievements.Add(new Achievement() { Id = 4, Name = "Noodlemeister", Description = "Du hast das zwanzigste Mal mit der Noodle App gelernt!", IsAchieved = false, Progress = 0 });
            newUser.Achievements.Add(new Achievement() { Id = 5, Name = "Klausurbereit", Description = "Du hast das fünfzigste Mal mit der Noodle App gelernt!", IsAchieved = false, Progress = 0 });

            this.users.Add(newUser);
            newUser.GuidChanged += GuidChangedEventHandler;
        }

        public bool UpdateUser(UserData newValues)
        {
            UserData searchResult;
            int index;

            if(newValues.Id != 0)
            {
                searchResult = UserService.Instance.Users.FirstOrDefault(x => x.Id == newValues.Id);

                index = UserService.Instance.Users.IndexOf(searchResult);

                if(searchResult != null && index != -1)
                {
                    UpdateHelper.UpdateObject(newValues, Users[index]);
                    WriteUserToDb(Users[index], false);
                    return true;
                } else
                {
                    return false;
                }
            } else
            {
                return false;
            }
        }

        public bool UpdateStudyUnit(UserData accessedUser, StudyUnit newValues)
        {
            if(newValues.Id != 0 && accessedUser.StudyUnits.Count != 0)
            {
                UpdateHelper.UpdateObject(newValues, accessedUser.StudyUnits[newValues.Id - 1]);
                WriteUserToDb(accessedUser, false);
                return true;
            } else
            {
                return false;
            }
        }

        public bool UpdateAchievement(UserData accessedUser, Achievement newValues)
        {
            if(newValues.Id != 0 && accessedUser.Achievements.Count != 0)
            {
                UpdateHelper.UpdateObject(newValues, accessedUser.Achievements[newValues.Id - 1]);

                WriteUserToDb(accessedUser, false);
                return true;
            } else
            {
                return false;
            }
        }

        private void GuidChangedEventHandler(object sender, EventArgs e)
        {
            WriteUserToDb((UserData)sender,false);
        }

        public void WriteUsersToDb()
        {
            if(users != null)
            {
                foreach (UserData currentUser in users)
                {
                    dbAccessor.Update<UserData>(currentUser.Id, currentUser);
                }
            }

        }

        public void WriteUserToDb(UserData user, bool isNewUser)
        {
            if (!isNewUser)
            {
                dbAccessor.Update<UserData>(user.Id, user);
            } else
            {
                dbAccessor.InsertData(user);
            }
            
        }

        public void DeleteUserFromDb(UserData user)
        {
            dbAccessor.Delete(user);
        }








    }
}